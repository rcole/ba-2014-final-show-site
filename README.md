# BA Final Show Site

Here's the repo for the final show site. By sharing the code here with Git anyone who wants to can help out.

##Here's how to get started.

* First install Git on your machine. Instructions are here. Git is a version tracking system that allows you to manage code between multiple users, fall back to earlier versions etc. It's cool once you get going with it!  
[Follow the instructions here.](http://git-scm.com/downloads)  

* Install a localhost server like [MAMP](https://www.mamp.info/en/) or [XAMPP](https://www.apachefriends.org/index.html).  
We'll need this to get some things with the map's JS working. Once you have this installed you'll need to navigate to the htdocs folder that is inside the app directory in applications. Have this visible in the finder. 

* Open up Terminal.app (if you're on a mac) and type ```cd ``` then a space, and then drag the htdocs folder into the termial window and drop it there. That should copy the path to the folder into the command line and look something like this :  
 ```cd /Applications/XAMPP/xamppfiles/htdocs```.   
* Now hit enter to run the command. ```cd``` stands for "change directory" and moves the commandline cursor into the folder that follows.

* Now that you are "in" the htdocs directory, clone the code from the remote repository by running this command in Terminal.  
```git clone https://rcole@bitbucket.org/rcole/ba-2014-final-show-site.git```  
That should get all the remote code into your machine and initialize a new git repo there. 

* Fire up XAMPP or MAMP and hit the "go to application" (or similar) button in the UI. That should bring up a browser window with a url like ```http://localhost:81``` in the address bar. The exact URL might be a bit different. 

* Now click into the folder that was just created by the cloning the repo. Most likely it's called ```ba_site```. There you go!


##Working with Git

When you are are working in a Git repo there is a workflow to follow. 

The easiest way to use Git is with the command line (aka Terminal.app). Don't be scared it's really not that bad!

___
You will type commands into terminal to tell Git what to do. 

There are a few primary things you will be doing. 

###Commit
The idea is that each time you finish some task, like styling a page, adding some HTML, fixing a broken link, etc you make a "commit".  

A commit represents a singular completed task. Commits are saved locally in the repo, allowing you to go back and "undo" things as well as jump back and forth in the timeline of a project. Think of Carabiners in rock climbing.  
![Alt text](http://qph.is.quoracdn.net/main-qimg-a9e5e574e6031f8a3b3954f156d318c2?convert_to_webp=true)

Make a commit with this command: ```git commit -a -m "a message for the commit"```  
  
This will make a new commit from the current state of the project, and add the message "a message for the commit". Obviously this should explain what you did.

###Push

When you make commits while working they are saved locally (on your own machine).  

In order for others to gain access to your changes you need ot push the commits to a remote server. The remote server is already setup so all you need to do it use the command ```git push```.  

A "push" is when you upload your code.  

###Pull

Likewise if a classmate, or I have made some changes, you can do ```git pull``` to pull down the changes which have been made on the remote server.  

You should do this every time you start a work session to make sure you have the latest code.   

   
###Status
It's not a required step, but sometimes you need to see what files you have worked on. The command ```git status``` will give you a list of all edited files. 

###Adding files
Whenever you add a new file (image, html file, css file etc), you will need to tell Git to track that file.  

You can do this with the command ```git add path/to/file```. Alternatively you can tell it to add all new files with ```git add .```

___

####Here's a typical work session:

* ```cd /Applications/XAMPP/xamppfiles/htdocs/ba_site``` > gets you into the right directory. It might be a little different for you. Do the draggy thing to get the right path. 

* ```git pull``` > grabs the latest code from the repo  
  
* *… do some awesome coding here …*  

* ```git commit -a -m "I did some awesome coding"``` > save that work into a commit

* *… do some more awesome coding …*  

* ```git commit -a -m "more awesome features!"``` > save more awesome work

* ```git push``` > upload those commits to the remote repo